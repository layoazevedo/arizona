<?php

/**
* @author  [Layo Demetrio] <layoazevedo@gmail.com>
* @package [Config]
* @since   [2017-09-05]
* @return  [Array mixed]
*/

return [
    'contentType' => [
        'application/json' => [
            'type' => 'application/json',
            'extension' => '.json'
        ],
        'application/xml' => [
            'type' => 'application/xml',
            'extension' => '.xml'
        ],
        'application/pdf' => [
            'type' => 'application/pdf',
            'extension' => '.pdf'
        ],
        'text/html' => [
            'type' => 'text/html',
            'extension' => '.html'
        ],
        'text/csv' => [
            'type' => 'text/csv',
            'extension' => '.csv'
        ],
        'text/plain' => [
            'type' => 'text/plain',
            'extension' => '.txt'
        ],
    ],

    'fileName' => [
        'arizona' => 'arizona'
    ],
];
