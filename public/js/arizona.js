var ArizonaConfig = {
    generateFile: $('#generateFile'),
    filePath:     '/generate-csv',

    /**
     * @method   [generateCsv]
     * @author   [Layo Demetrio] <layoazevedo@gmail.com>
     * @since    [2017-09-05]
     * @version  [1.0]
     * @return   [void]
     */
    generateCsv: function() {
        "use strict";

        $(ArizonaConfig.generateFile.selector).click(function() {
            window.location.href = ArizonaConfig.filePath;
        });
    },

    init: function () {
        ArizonaConfig.generateCsv();
    }
}

$(document).ready(function() {
    "use strict";

    ArizonaConfig.init();
});
