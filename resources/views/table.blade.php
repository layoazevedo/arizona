<table data-toggle="table" data-show-refresh="true" data-show-toggle="true"
    data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true"
    data-sort-name="name" data-sort-order="desc" class="table table-hover table-striped">

    <thead class="thead-default">
        <th style="">
            <div class="th-inner sortable">ID</div>
            <div class="fht-cell"></div>
        </th>

        <th style="">
            <div class="th-inner sortable">Country code</div>
            <div class="fht-cell"></div>
        </th>

        <th style="">
            <div class="th-inner sortable">Country name</div>
            <div class="fht-cell"></div>
        </th>
    </thead>
