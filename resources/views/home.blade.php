<!DOCTYPE html>
<html lang="en">
  @include('head')
  <body>
      <div class="container">
        @include('nav')
            @include('table')
                <tbody>
                    @foreach ($country as $value)
                        <tr>
                            <td style="">{{ $value->idCountry }}</td>
                            <td style="">{{ $value->countryCode }}</td>
                            <td style="">{{ $value->countryName }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </nav>

        <div class="jumbotron">
            <button class="btn btn-lg btn-success" id="generateFile">Gerar arquivo CSV
                <span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span>
            </button>
          </div>
    </div> <!-- /container -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="http://localhost:8005/js/btg360/ros/bootstrap-table.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  </body>
</html>
