<?php

namespace App\Http\Controllers;

use App\Arizona\Service\CountryService;
use App\Http\Controllers\Controller;
use App\Arizona\Type\ColumnsType;
use App\Arizona\Type\ArizonaType;
use Illuminate\Http\Request;

class GenerateCsvController extends Controller
{
    private $countryService;

    public function __construct(CountryService $countryService)
    {
        $this->countryService = $countryService;
    }

    /**
    * @author  [Layo Demetrio] <layoazevedo@gmail.com>
    * @see     [https://laravel.com/docs/5.4/eloquent]
    * @package [App\Http\Controllers]
    * @since   [2017-09-05]
    * @return  void
    */
    public function show()
    {
        $this->countryService->buildHeader(ArizonaType::CSV);
        $this->countryService->generateCsv([
            ColumnsType::ID_COUNTRY,
            ColumnsType::COUNTRY_CODE,
            ColumnsType::COUNTRY_NAME]
        );
    }
}
