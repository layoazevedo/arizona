<?php

namespace App\Http\Controllers;

use App\Arizona\Service\CountryService;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    private $countryService;

    public function __construct(CountryService $countryService)
    {
        $this->countryService = $countryService;
    }

    /**
    * @author  [Layo Demetrio] <layoazevedo@gmail.com>
    * @see     [https://laravel.com/docs/5.4/eloquent]
    * @package [App\Http\Controllers]
    * @since   [2017-09-05]
    * @return  Response
    */
    public function show()
    {
        return view('home', [
            'country' => $this->countryService->getCountry()
        ]);
    }
}
