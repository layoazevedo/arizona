<?php

namespace App\Arizona\Type;

interface ColumnsType
{
    /**
    * @author  [Layo Demetrio] <layoazevedo@gmail.com>
    * @package [App\Arizona\Type] [Interface]
    * @since   [2017-09-05]
    */
    const ID_COUNTRY   = 'idCountry';
    const COUNTRY_CODE = 'countryCode';
    const COUNTRY_NAME = 'countryName';
}
