<?php

namespace App\Arizona\Type;

interface ArizonaType
{
    /**
    * @author  [Layo Demetrio] <layoazevedo@gmail.com>
    * @package [App\Arizona\Type] [Interface]
    * @since   [2017-09-05]
    */
    const CONTENT_TYPE = 'contentType';
    const DEFAULT_NAME = 'arizona';
    const FILE_NAME    = 'fileName';
    const EXTENSION    = 'extension';
    const TYPE         = 'type';
    const JSON         = 'application/json';
    const HTML         = 'text/html';
    const TEXT         = 'text/plain';
    const CSV          = 'text/csv';
    const XML          = 'application/xml';
    const PDF          = 'application/pdf';
}
