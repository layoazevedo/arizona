<?php

namespace App\Arizona\Service;

use App\Arizona\Model\GenerateCsvModel;
use App\Arizona\Factory\HeaderFactory;
use App\Arizona\Model\CountryModel;

class CountryService
{
    private $collectionService;
    private $generateCsvModel;
    private $headerFactory;
    private $countryModel;

    public function __construct(
        CountryModel $countryModel,
        HeaderFactory $headerFactory,
        GenerateCsvModel $generateCsvModel
    ) {
        $this->generateCsvModel = $generateCsvModel;
        $this->headerFactory    = $headerFactory;
        $this->countryModel     = $countryModel;
    }

    /**
    * @author  [Layo Demetrio] <layoazevedo@gmail.com>
    * @see     [https://laravel.com/docs/5.4/eloquent]
    * @package [App\Arizona\Service]
    * @since   [2017-09-05]
    * @return  \Illuminate\Database\Eloquent\Collection
    */
    public function getCountry()
    {
        return $this->countryModel->getCountryAll();
    }

    /**
    * @author  [Layo Demetrio] <layoazevedo@gmail.com>
    * @package [App\Arizona\Service]
    * @since   [2017-09-05]
    * @return  String $header
    */
    public function buildHeader($header)
    {
        return $this->headerFactory->build($header);
    }

    /**
    * @author  [Layo Demetrio] <layoazevedo@gmail.com>
    * @package [App\Arizona\Service]
    * @since   [2017-09-05]
    * @return  array $columns
    */
    public function generateCsv(array $columns)
    {
        $this->generateCsvModel->setColumns($columns);
        $this->collectionService = $this->countryModel->getCountryAll()->toArray();

        foreach ($this->collectionService as $row) {
            $this->generateCsvModel->appendFile($this->generateCsvModel->getFile(), $row);
        }

        return $this->collectionService;
    }
}
