<?php

namespace App\Arizona\Model;

class GenerateCsvModel
{
    private $file;

    public function __construct()
    {
        $this->file = fopen('php://output', 'w');
    }

    /**
    * @author  [Layo Demetrio] <layoazevedo@gmail.com>
    * @package [App\Arizona\Model]
    * @since   [2017-09-05]
    * @param   array $columns
    * @return  Stream
    */
    public function setColumns(array $columns = [])
    {
        return fputcsv($this->file, $columns);
    }

    /**
    * @author  [Layo Demetrio] <layoazevedo@gmail.com>
    * @package [App\Arizona\Model]
    * @since   [2017-09-05]
    * @param   Object $data, String $row
    * @return  Stream
    */
    public function appendFile($data, $row)
    {
        return fputcsv($data, $row);
    }

    /**
    * @author  [Layo Demetrio] <layoazevedo@gmail.com>
    * @package [App\Arizona\Model]
    * @since   [2017-09-05]
    * @return  Object
    */
    public function getFile()
    {
        return $this->file;
    }
}
