<?php

namespace App\Arizona\Model;

use Illuminate\Database\Eloquent\Model;

class CountryModel extends Model
{
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    protected $table      = 'country';
    public    $timestamps = false;
    private   $result;

    /**
    * @author  [Layo Demetrio] <layoazevedo@gmail.com>
    * @see     [https://laravel.com/docs/5.4/eloquent]
    * @package [App\Arizona\Model]
    * @since   [2017-09-05]
    * @return  \Illuminate\Database\Eloquent\Collection
    */
    public function getCountryAll()
    {
        return CountryModel::get([
            'id as idCountry',
            'country_code as countryCode',
            'country_name as countryName',
        ]);
    }

}
