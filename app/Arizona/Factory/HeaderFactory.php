<?php

namespace App\Arizona\Factory;

use App\Arizona\Type\ArizonaType;

class HeaderFactory
{
    private $header;

    public function __construct()
    {
        $this->header   = require dirname(__FILE__) . '../../../../config/header.php';
        $this->fileName = $this->header[ArizonaType::FILE_NAME][ArizonaType::DEFAULT_NAME];
    }

    /**
    * @author  [Layo Demetrio] <layoazevedo@gmail.com>
    * @package [App\Arizona\Factory]
    * @since   [2017-09-05]
    * @param   String $contentType
    * @return  Response
    */
    public function build($contentType)
    {
        header('Content-type: '.
            $this->header[ArizonaType::CONTENT_TYPE][$contentType][ArizonaType::TYPE]
        );
        header('Content-Disposition: attachment; filename="'.$this->fileName.
            $this->header[ArizonaType::CONTENT_TYPE][$contentType][ArizonaType::EXTENSION]
        );

        return $this->noCacheControl();
    }

    /**
    * @author  [Layo Demetrio] <layoazevedo@gmail.com>
    * @package [App\Arizona\Factory]
    * @since   [2017-09-05]
    * @return  void
    */
    private function noCacheControl()
    {
        header('Pragma: no-cache');
        header('Expires: 0');
    }
}
